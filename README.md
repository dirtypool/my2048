## 2048游戏笔记

1. Js控制css

   ```javascript
   var grid_cell = $("#grid_cell_"+i+"_"+j);
   //通过ID赋值对象
   grid_cell.css('top',get_pos_top(i,j));
   //修改top值
   ```

2. div嵌套

   如果父div的position: relative，子div的position: absolute，且父div设置了padding，子div会受padding影响。

   但是子div设置top，left等值直接相对于父div，不受padding值的影响。

3. JavaScript只支持一维数组，要声明二维数组只能这么写个二重循环

4. 当整个程序加载完毕以后，运行函数

   ```javascript
   $(document).ready(function(){
       //
   })
   ```


5. Viewport

   放在head里

   ```html
   <meta name = "viewport"
       content = "
         width = [pixel_value（像素值）|device-width（屏幕的宽）],  
         height = [pixel_value|device-height],
         inital-scale = float_value,   应用程序启动后缩放尺度
         minimum-scale = float_value,  用户缩小应用程序的最小尺度值
         maximum-scale = float_value,  用户放大应用程序的最大尺度值
         user-scalable = [yes|no]      用户是否可以通过手势缩放整个应用程序"/>
   ```

6. touch

   touch和上下左右的监听一致，也是通过event事件的数据来确定touch的始（x,y）和终（x,y）

   由于touches是数组（存储多点触控的信息，单点触控就只需要数组的第一个数据）

   ```javascript
   //开始
   $(document).addEventListener("touchstart",function (e) { 
   	startx=event.touches[0].pageX; 
   	starty=event.touches[0].pageY;
   });
   //结束
   $(document).addEventListener("touchend",function (e) { 
   	endx=event.changedTouches[0].pageX; 
   	endy=event.changedTouches[0].pageY;
       //对touch动作的判断在这个中进行
   });
   ```

   **注意：在屏幕坐标系中 Y 轴是竖直向下的** 

